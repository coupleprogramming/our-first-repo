require "gtk3"

app = Gtk::Application.new("org.gtk.example", :flags_none)

app.signal_connect "activate" do |application|
  window = Gtk::ApplicationWindow.new(application)
  window.set_title("My Ruby Program")
  window.set_default_size(640, 480)
  window.show_all
end

puts app.run